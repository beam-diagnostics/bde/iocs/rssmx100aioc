###############################################################################
#
# PBI system   : R&S SMA100A/SMB100A series signal generator
# Location     : lab
#
# Support      : https://jira.esss.lu.se/projects/PBITECH
# Wiki         : https://confluence.esss.lu.se/display/PBITECH
#
###############################################################################

# location of the system (section-subsection)
# lab example
epicsEnvSet("RACKROW",                      "PBILAB")
# acquisition unit logical name
epicsEnvSet("ACQ_UNIT",                     "RSSMX")
# acquisition device ID or name
epicsEnvSet("ACQ_DEVID",                    "RSSMX")
# acquisition device model
epicsEnvSet("ACQ_MODEL",                    "RSSMA100")
# acquisition device serial number (empty means any)
#epicsEnvSet("ACQ_SERNO",                    "")
# acquisition device IP address and port number
epicsEnvSet("ACQ_IPADDR",                   "172.30.150.64")
epicsEnvSet("ACQ_PORTNR",                   "5025")

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
# epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")

# include the main startup file
cd startup
< main.cmd
